package com.sonepar.labs.biskot.domain.cart.engine;

import com.sonepar.labs.biskot.domain.errors.BiskotBusinessException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

class LinePriceTest {

    private static final Long PRODUCT_ID = 1L;

    @Test
    void calculateLinePrice() {
        Assertions.assertThat(LinePrice.domainBuilder()
                .quantityInStock(10)
                .quantity(9)
                .unitPrice(BigDecimal.valueOf(33.23))
                .build()).isNotNull();
    }

    @Test
    void calculateLinePrice_Quantity_In_Stock_Exceeded() {
        org.junit.jupiter.api.Assertions.assertThrows(BiskotBusinessException.class, () -> {
            LinePrice.domainBuilder()
                    .quantityInStock(10)
                    .quantity(11)
                    .unitPrice(BigDecimal.valueOf(33.23))
                    .build();
        });
    }

    @Test
    void testCalculateLinePrice() {
        Assertions.assertThat(LinePrice.domainBuilder()
                .quantityInStock(10)
                .quantity(9)
                .unitPrice(BigDecimal.valueOf(33.23))
                .build().calculateLinePrice()).isEqualByComparingTo(BigDecimal.valueOf(299.07));
    }

}
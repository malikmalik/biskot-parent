package com.sonepar.labs.biskot.domain.cart.model;

import com.sonepar.labs.biskot.domain.errors.BiskotBusinessException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;


class CartTest {

    private static final String CART_ID = "7e076f36-faca-4ae2-b961-8890bd11a5a6";

    @Test
    void testTotalPrice() {
        Cart cart = Cart.domainBuilder().cartId(CART_ID).items(Arrays.asList(Item.domainBuilder()
                .linePrice(BigDecimal.valueOf(100))
                .build())).build();
        Assertions.assertThat(cart).isNotNull();
    }

    @Test
    void testTotalPriceExceeded() {

        org.junit.jupiter.api.Assertions.assertThrows(BiskotBusinessException.class, () -> {
            Cart.domainBuilder().cartId(CART_ID).items(Arrays.asList(Item.domainBuilder()
                            .linePrice(BigDecimal.valueOf(101))
                    .build())).build();
        });
    }

    @Test
    void testTotalDifferentItems() {
        Cart cart = buildCart();
        Assertions.assertThat(cart).isNotNull();
    }

    @Test
    void testTotalDifferentItemsExceeded() {
        org.junit.jupiter.api.Assertions.assertThrows(BiskotBusinessException.class, () -> {
            // Cart with 2 items
            Cart cart = buildCart();
            // Add 2 items
            Cart cartWith3Items = Cart.domainBuilder().fromCart(cart).putItem(Item.domainBuilder().productId(3L).quantity(2).linePrice(BigDecimal.valueOf(11.1)).build()).build();
            Cart.domainBuilder().fromCart(cartWith3Items).putItem(Item.domainBuilder().productId(4L).quantity(1).linePrice(BigDecimal.valueOf(11.1)).build()).build();
        });
    }

    /**
     * Cart with 2 items
     * @return cart
     */
    private Cart buildCart() {
        return Cart.domainBuilder()
                .cartId(CART_ID)
                .items(new ArrayList<>(Arrays.asList(
                        Item.domainBuilder()
                                .productId(1L)
                                .quantity(2)
                                .linePrice(BigDecimal.valueOf(6.22))
                                .build(),
                        Item.domainBuilder()
                                .productId(2L)
                                .quantity(4)
                                .linePrice(BigDecimal.valueOf(12.16))
                                .build())
                ))
                .build();
    }
}
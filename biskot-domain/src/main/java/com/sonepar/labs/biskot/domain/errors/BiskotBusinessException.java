package com.sonepar.labs.biskot.domain.errors;

public class BiskotBusinessException extends BusinessException{

    private static final long serialVersionUID = 1L;

    public BiskotBusinessException(Builder builder) {
        super(builder);
    }

    public BiskotBusinessException(String message, String... params) {
        this(new Builder(message, params));
    }

    public static void throwBusinessException(String message, String... params) {
        throw new BiskotBusinessException(new BusinessException.Builder(message, params));
    }
}

package com.sonepar.labs.biskot.domain.errors;

import org.springframework.http.HttpStatus;

public class BiskotTechnicalException extends TechnicalException {


    public BiskotTechnicalException(String message, String... params) {
        super(message, params);
    }

    public BiskotTechnicalException(String message, Throwable cause, String... params) {
        super(message, cause, params);
    }

    public BiskotTechnicalException(HttpStatus httpStatus, String message, String... params) {
        super(httpStatus, message, params);
    }

    public BiskotTechnicalException(HttpStatus httpStatus, String message, Throwable cause, String... params) {
        super(httpStatus, message, cause, params);
    }
}

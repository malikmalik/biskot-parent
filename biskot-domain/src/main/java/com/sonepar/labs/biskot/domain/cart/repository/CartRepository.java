package com.sonepar.labs.biskot.domain.cart.repository;

import com.sonepar.labs.biskot.domain.cart.model.Cart;

public interface CartRepository {

    /**
     * Find Cart by id
     * @param cartId cart Identifier
     * @return Cart details
     */
    Cart findById(String cartId);

    /**
     * Create or Update Cart
     * @param cart cart
     * @return Cart created or updated
     */
    Cart saveCart(Cart cart);
}

package com.sonepar.labs.biskot.domain.errors;

public class BiskotErrorConstants {

    // Biskot Errors
    public static final String ERR_CART_NOT_FOUND = "ERR-CART-001";
    public static final String ERR_CART_TOTAL_PRICE_EXCEEDED = "ERR-CART-002";
    public static final String ERR_CART_DIFFERENT_ITEMS_EXCEEDED = "ERR-CART-003";
    public static final String ERR_CART_QUANTITY_STOCK_EXCEEDED = "ERR-CART-004";

    // Product Errors
    public static final String ERR_PRODUCT_NOT_FOUND = "ERR-PROD-001";
    public static final String ERR_PROD_TECH="ERR-PROD-TECH-001";

    }


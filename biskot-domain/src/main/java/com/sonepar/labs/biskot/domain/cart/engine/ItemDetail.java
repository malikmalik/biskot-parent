package com.sonepar.labs.biskot.domain.cart.engine;

import com.sonepar.labs.biskot.domain.product.model.Product;
import lombok.*;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ItemDetail {

    private Product product;
    private Integer quantity;
    private BigDecimal linePrice;
}

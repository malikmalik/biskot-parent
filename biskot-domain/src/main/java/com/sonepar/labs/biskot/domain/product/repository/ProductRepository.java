package com.sonepar.labs.biskot.domain.product.repository;

import com.sonepar.labs.biskot.domain.product.model.Product;

public interface ProductRepository {

    /**
     * Find Product by identifier
     * @param productId product identifier
     * @return Product details
     */
    Product findById(Long productId);
}

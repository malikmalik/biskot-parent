package com.sonepar.labs.biskot.domain.cart.model;

import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Optional;

@Getter
@ToString
public class Item {

    /** Product Identifer **/
    private Long productId;

    /** Qantity of product in item **/
    private Integer quantity;

    /** Line Price : unitPrice * Quantity **/
    private BigDecimal linePrice;

    private Item(DomainBuilder builder) {
        this.productId = builder.productId;
        this.quantity = builder.quantity;
        this.linePrice = builder.linePrice;
    }


    public static DomainBuilder domainBuilder() {
        return new DomainBuilder();
    }

    public static class DomainBuilder {
        private Long productId;
        private Integer quantity;
        private BigDecimal linePrice;

        DomainBuilder fromItem(Item item) {
            Optional.ofNullable(item).ifPresent(i -> Item.domainBuilder()
                    .productId(i.productId)
                    .quantity(i.quantity)
                    .linePrice(i.linePrice)
                    .build());
            return this;
        }

        public DomainBuilder productId(Long productId) {
            this.productId = productId;
            return this;
        }

        public DomainBuilder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public DomainBuilder linePrice(BigDecimal linePrice) {
            this.linePrice = linePrice;
            return this;
        }

        public Item build() {
            Item item = new Item(this);
            return item;
        }
    }

}

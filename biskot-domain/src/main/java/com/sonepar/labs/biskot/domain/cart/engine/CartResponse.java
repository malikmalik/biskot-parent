package com.sonepar.labs.biskot.domain.cart.engine;

import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CartResponse {

    private String cartId;
    private List<ItemDetail> itemDetails = new ArrayList<>();
    private BigDecimal totalPrice;


}

package com.sonepar.labs.biskot.domain.product.model;

import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@ToString
public class Product {

    private Long id;
    private String label;
    private BigDecimal unitPrice;
    private Integer quantityInStock;

    private Product(DomainBuilder builder) {
        this.id = builder.id;
        this.label = builder.label;
        this.unitPrice = builder.unitPrice;
        this.quantityInStock = builder.quantityInStock;
    }

    public static DomainBuilder domainBuilder() {
        return new DomainBuilder();
    }

    public static class DomainBuilder {
        private Long id;
        private String label;
        private BigDecimal unitPrice;
        private Integer quantityInStock;

        public DomainBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public DomainBuilder label(String label) {
            this.label = label;
            return this;
        }

        public DomainBuilder unitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
            return this;
        }

        public DomainBuilder quantityInStock(Integer quantityInStock) {
            this.quantityInStock = quantityInStock;
            return this;
        }

        public Product build() {
            Product product = new Product(this);
            return product;
        }
    }
}

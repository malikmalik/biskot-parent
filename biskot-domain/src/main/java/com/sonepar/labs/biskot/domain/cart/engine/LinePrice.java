package com.sonepar.labs.biskot.domain.cart.engine;

import com.sonepar.labs.biskot.domain.errors.BiskotBusinessException;
import com.sonepar.labs.biskot.domain.errors.BiskotErrorConstants;
import com.sonepar.labs.biskot.domain.errors.BusinessException;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Predicate;

@ToString
@Getter
public class LinePrice {

    /** Quantity in item **/
    private Integer quantity;

    /** Unit price of product **/
    private BigDecimal unitPrice;

    /** Quantity in Stock Product**/
    private Integer quantityInStock;

    private LinePrice(DomainBuilder builder) {
        this.quantity = builder.quantity;
        this.unitPrice = builder.unitPrice;
        this.quantityInStock = builder.quantityInStock;
    }

    public static DomainBuilder domainBuilder() {
        return new DomainBuilder();
    }
    public static class DomainBuilder {

        private Integer quantity;
        private BigDecimal unitPrice;
        private Integer quantityInStock;

        public DomainBuilder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public DomainBuilder unitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
            return this;
        }
        public DomainBuilder quantityInStock(Integer quantityInStock) {
            this.quantityInStock = quantityInStock;
            return this;
        }
        public LinePrice build() {
            LinePrice linePrice = new LinePrice(this);
            InvariantChecker.quantityInStockExceeded.test(linePrice);
            return linePrice;
        }
    }

    public BigDecimal calculateLinePrice() {
        return  Optional.ofNullable(this.unitPrice).orElse(BigDecimal.ZERO)
                .multiply(BigDecimal.valueOf(Optional.ofNullable(this.quantity).orElse(0)));
    }

    private static class InvariantChecker {
        final static private Predicate<LinePrice> quantityInStockExceeded = req -> {
            if (req.getQuantity() > req.getQuantityInStock()) {
                throw new BiskotBusinessException(new BusinessException.Builder(BiskotErrorConstants.ERR_CART_QUANTITY_STOCK_EXCEEDED));
            }

            return true;
        };
    }
}

package com.sonepar.labs.biskot.domain.cart.model;

import com.sonepar.labs.biskot.domain.errors.BiskotBusinessException;
import com.sonepar.labs.biskot.domain.errors.BiskotErrorConstants;
import com.sonepar.labs.biskot.domain.errors.BusinessException;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@Getter
@ToString
public class Cart {

    /** Cart Identifier **/
    private String cartId;

    /** List of Items **/
    private List<Item> items;

    /** Total Price of Cart **/
//    private BigDecimal totalPrice;
    public BigDecimal getTotalPrice() {
        return Optional.ofNullable(this.items).orElse(Collections.emptyList())
                .stream().map(Item::getLinePrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private Cart(DomainBuilder builder) {
        this.cartId = builder.cartId;
        //this.totalPrice = builder.totalPrice;
        this.items = builder.items;
    }

    public static DomainBuilder domainBuilder() {
        return new DomainBuilder();
    }

    public static class DomainBuilder {
        private String cartId;
        private List<Item> items = new ArrayList<>();
        //private BigDecimal totalPrice ;

        public DomainBuilder fromCart(Cart cart) {
            Optional.ofNullable(cart).ifPresent(c -> {
                this.cartId = c.getCartId();
                //this.totalPrice = c.getTotalPrice();
                this.items = c.getItems();
            });
            return this;
        }

        public DomainBuilder cartId(String cartId) {
            this.cartId = cartId;
            return this;
        }

        public DomainBuilder items(List<Item> items) {
            this.items = items;
            return this;
        }

        public DomainBuilder putItem(Item item) {
            this.items.stream().filter(i -> i.getProductId().equals(item.getProductId())).findFirst()
                    .ifPresent(e -> this.items.remove(e));
            this.items.add(item);
            //this.totalPrice = calculateTotalPrice();
            return this;
        }

//        public DomainBuilder totalPrice(BigDecimal totalPrice) {
//            this.totalPrice = totalPrice;
//            return this;
//        }

        public Cart build() {
            Cart cart = new Cart(this);
            InvariantChecker.mandatoryFields.test(cart);
            InvariantChecker.totalItemsExceeded.test(cart);
            InvariantChecker.totalPriceExceeded.test(cart);
            return cart;
        }
    }

    private static class InvariantChecker {

        final static private Predicate<Cart> mandatoryFields = req -> {
            if (req.getCartId() == null || req.getCartId().isEmpty()) {
                throw new BiskotBusinessException("Mandatory field");
            }

            return true;
        };

        final static private Predicate<Cart> totalPriceExceeded = req -> {
            if (Optional.ofNullable(req.getTotalPrice()).orElse(BigDecimal.ZERO).compareTo(BigDecimal.valueOf(100)) == 1) {
                throw new BiskotBusinessException(new BusinessException.Builder(BiskotErrorConstants.ERR_CART_TOTAL_PRICE_EXCEEDED));
            }

            return true;
        };

        final static private Predicate<Cart> totalItemsExceeded = req -> {
            if (req.items.size() > 3) {
                throw new BiskotBusinessException(new BusinessException.Builder(BiskotErrorConstants.ERR_CART_DIFFERENT_ITEMS_EXCEEDED));
            }

            return true;
        };
    }
}

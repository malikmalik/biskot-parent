package com.sonepar.labs.biskot.application.cart.create;

import com.sonepar.labs.biskot.application.cqrs.command.CommandV2;
import com.sonepar.labs.biskot.domain.cart.model.Cart;

public interface CreateCartCommand extends CommandV2<Cart, CreateCartRequest> {
}

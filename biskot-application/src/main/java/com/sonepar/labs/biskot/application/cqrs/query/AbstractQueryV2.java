package com.sonepar.labs.biskot.application.cqrs.query;

public abstract class AbstractQueryV2 <RESULT, REQUEST extends QueryRequestV2> implements QueryV2<RESULT, REQUEST> {

    @Override
    public RESULT execute(REQUEST request) {
        return doExecute(request);
    }

    public abstract RESULT doExecute(REQUEST request);
}

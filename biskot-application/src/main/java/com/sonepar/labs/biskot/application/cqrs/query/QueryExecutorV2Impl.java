package com.sonepar.labs.biskot.application.cqrs.query;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class QueryExecutorV2Impl implements QueryExecutorV2, ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public <RESULT, REQUEST extends QueryRequestV2> RESULT execute(Class<? extends QueryV2<RESULT, REQUEST>> queryClass, REQUEST request) {
        QueryV2<RESULT, REQUEST> query = applicationContext.getBean(queryClass);
        return query.execute(request);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

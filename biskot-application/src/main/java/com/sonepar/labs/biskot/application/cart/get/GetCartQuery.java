package com.sonepar.labs.biskot.application.cart.get;

import com.sonepar.labs.biskot.application.cqrs.query.QueryV2;
import com.sonepar.labs.biskot.domain.cart.engine.CartResponse;

public interface GetCartQuery extends QueryV2<CartResponse, GetCartRequest> {
}

package com.sonepar.labs.biskot.application.cqrs.command;

public interface CommandV2<RESULT, REQUEST extends CommandRequestV2> {

    RESULT execute(REQUEST request);
}

package com.sonepar.labs.biskot.application.cart.create;

import com.sonepar.labs.biskot.domain.cart.model.Cart;
import com.sonepar.labs.biskot.domain.cart.repository.CartRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class CreateCartCommandImpl implements CreateCartCommand {

    @Autowired
    private CartRepository cartRepository;

    @Override
    public Cart execute(CreateCartRequest createCartRequest) {
        Cart cart = cartRepository.saveCart(Cart.domainBuilder()
                        .cartId(UUID.randomUUID().toString())
                .build());

        return cart;
    }
}

package com.sonepar.labs.biskot.application.cqrs.command;

public abstract class AbstractCommandV2<RESULT, REQUEST extends CommandRequestV2> implements CommandV2<RESULT, REQUEST> {

    @Override
    public RESULT execute(REQUEST request) {
        RESULT result = doExecute(request);
        return result;
    }

    public abstract RESULT doExecute(REQUEST request);
}

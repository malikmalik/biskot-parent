package com.sonepar.labs.biskot.application.cqrs.command;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

//@Component
@Service
public class CommandExecutorV2Impl implements CommandExecutorV2, ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public <RESULT, REQUEST extends CommandRequestV2> RESULT execute(Class<? extends CommandV2<RESULT, REQUEST>> commandClass, REQUEST request) {
        CommandV2<RESULT, REQUEST> command = applicationContext.getBean(commandClass);
        return command.execute(request);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

package com.sonepar.labs.biskot.application.cart.get;

import com.sonepar.labs.biskot.domain.cart.engine.CartResponse;
import com.sonepar.labs.biskot.domain.cart.engine.ItemDetail;
import com.sonepar.labs.biskot.domain.cart.model.Cart;
import com.sonepar.labs.biskot.domain.cart.repository.CartRepository;
import com.sonepar.labs.biskot.domain.errors.BiskotBusinessException;
import com.sonepar.labs.biskot.domain.errors.BiskotErrorConstants;
import com.sonepar.labs.biskot.domain.errors.BusinessException;
import com.sonepar.labs.biskot.domain.product.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GetCartQueryImpl implements GetCartQuery {

    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private ProductRepository productRepository;

    @Override
    public CartResponse execute(GetCartRequest getCartRequest) {

        // 1. Get Cart
        Cart cart = Optional.ofNullable(cartRepository.findById(getCartRequest.getCartId()))
                .orElseThrow(() -> new BiskotBusinessException(new BusinessException.Builder(BiskotErrorConstants.ERR_CART_NOT_FOUND)));

        // 2. Get Item and Product details
        List<ItemDetail> itemDetailList = Optional.ofNullable(cart).map(Cart::getItems).orElse(Collections.emptyList()).stream()
                .map(item -> ItemDetail.builder()
                        .product(Optional.ofNullable(productRepository.findById(item.getProductId()))
                                .orElseThrow(() -> new BiskotBusinessException(new BusinessException.Builder(BiskotErrorConstants.ERR_CART_NOT_FOUND))))
                        .quantity(item.getQuantity())
                        .linePrice(item.getLinePrice())
                        .build()).collect(Collectors.toList());

        CartResponse cartResponse = CartResponse.builder()
                .cartId(getCartRequest.getCartId())
                .itemDetails(itemDetailList)
                .totalPrice(cart.getTotalPrice())
                .build();
        return cartResponse;
    }
}

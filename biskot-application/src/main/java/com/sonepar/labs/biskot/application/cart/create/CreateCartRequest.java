package com.sonepar.labs.biskot.application.cart.create;

import com.sonepar.labs.biskot.application.cqrs.command.CommandRequestV2;
import lombok.*;

@Getter
@NoArgsConstructor
@Builder
public class CreateCartRequest implements CommandRequestV2 {

}

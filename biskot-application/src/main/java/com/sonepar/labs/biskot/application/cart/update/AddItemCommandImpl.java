package com.sonepar.labs.biskot.application.cart.update;

import com.sonepar.labs.biskot.domain.cart.engine.LinePrice;
import com.sonepar.labs.biskot.domain.cart.model.Cart;
import com.sonepar.labs.biskot.domain.cart.model.Item;
import com.sonepar.labs.biskot.domain.cart.repository.CartRepository;
import com.sonepar.labs.biskot.domain.errors.BiskotBusinessException;
import com.sonepar.labs.biskot.domain.errors.BiskotErrorConstants;
import com.sonepar.labs.biskot.domain.errors.BusinessException;
import com.sonepar.labs.biskot.domain.product.model.Product;
import com.sonepar.labs.biskot.domain.product.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class AddItemCommandImpl implements AddItemCommand{

    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Cart execute(AddItemRequest addItemRequest) {

        // 1. Get Cart
        Cart cart = Optional.ofNullable(cartRepository.findById(addItemRequest.getCartId()))
                .orElseThrow(() -> new BiskotBusinessException(new BusinessException.Builder(BiskotErrorConstants.ERR_CART_NOT_FOUND)));

        // 2. Get Product
        Product product = Optional.ofNullable(productRepository.findById(addItemRequest.getProductId()))
                .orElseThrow(() -> new BusinessException(new BusinessException.Builder(BiskotErrorConstants.ERR_PRODUCT_NOT_FOUND)));

        // 3. Add Item
        return Optional.ofNullable(product).map(prod -> {
            Cart cartToSave = Cart.domainBuilder().fromCart(cart)
                    .putItem(Item.domainBuilder()
                            .productId(addItemRequest.getProductId())
                            .quantity(addItemRequest.getQuantity())
                            .linePrice(LinePrice.domainBuilder()
                                    .quantity(addItemRequest.getQuantity())
                                    .unitPrice(prod.getUnitPrice())
                                    .quantityInStock(prod.getQuantityInStock())
                                    .build().calculateLinePrice())
                    .build())
                    .build();
            cartRepository.saveCart(cartToSave);
            return cart;
        }).orElse(null);
    }
}

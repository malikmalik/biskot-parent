package com.sonepar.labs.biskot.application.cart.get;

import com.sonepar.labs.biskot.application.cqrs.query.QueryRequestV2;
import lombok.*;

@Getter
@ToString
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class GetCartRequest implements QueryRequestV2 {

    private String cartId;
}

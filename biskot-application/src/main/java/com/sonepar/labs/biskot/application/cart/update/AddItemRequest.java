package com.sonepar.labs.biskot.application.cart.update;

import com.sonepar.labs.biskot.application.cqrs.command.CommandRequestV2;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class AddItemRequest implements CommandRequestV2 {

    private String cartId;
    private Long productId;
    private Integer quantity;
}

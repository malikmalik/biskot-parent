package com.sonepar.labs.biskot.application.cqrs.command;

public interface CommandExecutorV2 {

    <RESULT, REQUEST extends CommandRequestV2> RESULT execute(Class<? extends CommandV2<RESULT, REQUEST>> commandClass, REQUEST request);
}

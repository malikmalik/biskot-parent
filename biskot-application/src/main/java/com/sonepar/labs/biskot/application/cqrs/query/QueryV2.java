package com.sonepar.labs.biskot.application.cqrs.query;

public interface QueryV2<RESULT, REQUEST extends QueryRequestV2> {

    RESULT execute(REQUEST request);
}

package com.sonepar.labs.biskot.application.cart.update;

import com.sonepar.labs.biskot.application.cqrs.command.CommandV2;
import com.sonepar.labs.biskot.domain.cart.model.Cart;

public interface AddItemCommand extends CommandV2<Cart, AddItemRequest> {
}

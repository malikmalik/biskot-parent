package com.sonepar.labs.biskot.application.cqrs.query;

public interface QueryExecutorV2 {

    <RESULT, REQUEST extends QueryRequestV2> RESULT execute(Class<? extends QueryV2<RESULT, REQUEST>> queryClass, REQUEST request);
}

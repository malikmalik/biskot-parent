package com.sonepar.labs.biskot.infrastructure.rest.product;

import com.sonepar.labs.biskot.domain.product.model.Product;
import com.sonepar.labs.biskot.domain.product.repository.ProductRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile(value = "!mock-search-product")
public class ProductRepositoryImpl implements ProductRepository {
    @Override
    public Product findById(Long productId) {
        // TODO : implement rest client to microservice Product
        return null;
    }
}

package com.sonepar.labs.biskot.infrastructure.rest.product;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonepar.labs.biskot.domain.errors.BiskotErrorConstants;
import com.sonepar.labs.biskot.domain.errors.BiskotTechnicalException;
import com.sonepar.labs.biskot.domain.product.model.Product;
import com.sonepar.labs.biskot.domain.product.repository.ProductRepository;
import com.sonepar.labs.biskot.infrastructure.rest.product.response.ProductResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Optional;

@Repository
@Profile(value = "mock-get-product")
@Primary
@Slf4j
public class ProductRepositoryMockImpl implements ProductRepository {

    @Autowired
    private ObjectMapper objectMapper;
    @Override
    public Product  findById(Long productId) {
        ResponseEntity<ProductResponse> responseEntity = null;
        try {
            log.debug("Mock Get Product Service by id  = {}", productId);
            ProductResponse response = objectMapper.readValue(new ClassPathResource("mock-responses/product-"+productId+".json")
                    .getInputStream(), new TypeReference<ProductResponse>() {
            });

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
            log.debug("Mock Get Product Response : {}", new ObjectMapper().writeValueAsString(responseEntity));

        } catch (IOException e) {
            throw new BiskotTechnicalException(BiskotErrorConstants.ERR_PROD_TECH, e);
        }

        return Optional.ofNullable(responseEntity).map(HttpEntity::getBody)
                .map(productResponse -> Product.domainBuilder()
                        .id(productResponse.getId())
                        .label(productResponse.getLabel())
                        .unitPrice(productResponse.getUnitPrice())
                        .quantityInStock(productResponse.getQuantityInStock())
                        .build())
                .orElse(null);
    }
}

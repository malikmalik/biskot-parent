package com.sonepar.labs.biskot.infrastructure.rest.product.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("label")
    private String label;
    @JsonProperty("unit_price")
    private BigDecimal unitPrice;
    @JsonProperty("quantity_in_stock")
    private Integer quantityInStock;
}

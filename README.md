# **Projet Sonepar Biskot**

La stack technique de l'application : Java 11 ou Java 8, SpringBoot 2.2.12, Spring 5, Base de données H2 

Les étapes pour démarrer l'application


## Prérequis : 
installer une jdk 8 ou une version récente.
installer maven en version récente tel que 3.3.6

## Build application :

Se positionner à la racine du projet
```
 mvn clean install 
```

## Run application

```
cd biskot-exposition/target/
java -jar biskot-exposition-0.0.1-SNAPSHOT.jar
```
## Test application
 Une fois l'application démarrée, on devrait avoir des informations comme ceci
 ```
 Application crm is running! Access URLs:
        Local:          http://localhost:8889
        External:       http://192.168.0.15:8889
        Swagger :       http://192.168.0.15:8889/swagger-ui.html
        Active profiles:        swagger
```
Il suffit ensuite de lancer dans un navigateur l'url : http://localhost:8889/swagger-ui.html
pour accéder au swagger qui nous permet de tester notre application.

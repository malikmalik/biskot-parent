package com.sonepar.labs.biskot.exposition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication(scanBasePackages = {"com.sonepar.labs.biskot"})
public class CrmApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(CrmApplication.class);


    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(CrmApplication.class, args);

        Environment env = configurableApplicationContext.getEnvironment();
        LOGGER.info("\n----------------------------------------------------------\n\t" +
                        "Application {} is running! Access URLs:\n\t" + "Local: \t\thttp://localhost:{}\n\t" +
                        "External: \thttp://{}:{}\n\t" +
                        "Swagger : \thttp://{}:{}/swagger-ui.html\n\t" +
                        "Active profiles: \t{}\t\n" +
                        "----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                StringUtils.arrayToCommaDelimitedString(env.getActiveProfiles())
        );
    }
}

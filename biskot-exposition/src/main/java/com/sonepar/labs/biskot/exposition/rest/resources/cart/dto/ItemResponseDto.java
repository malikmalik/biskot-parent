package com.sonepar.labs.biskot.exposition.rest.resources.cart.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class ItemResponseDto {

    @JsonProperty("product_id")
    private Long productId;

    @JsonProperty("product_label")
    private String productLabel;

    @JsonProperty("quantity")
    private Integer quantity;

    @JsonProperty("unit_price")
    private Double unitPrice;

    @JsonProperty("line_price")
    private Double linePrice;
}

package com.sonepar.labs.biskot.exposition.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@Profile("swagger")
@EnableSwagger2
public class SwaggerConfig {

    public static final String DEFAULT_INCLUDE_PATTERN = "/api/.*";

    @Bean
    public Docket customImplementation(CrmProperties crmProperties)
    {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(regex(DEFAULT_INCLUDE_PATTERN))
                .build()
                .apiInfo(apiInfo(crmProperties));

    }



    /**
     * API Info as it appears on the swagger-ui page
     */
    private ApiInfo apiInfo(CrmProperties crmProperties)
    {
        return new ApiInfoBuilder()
                .title(crmProperties.getSwagger().getTitle())
                .description(crmProperties.getSwagger().getDescription())
                .version(crmProperties.getSwagger().getVersion())
                .licenseUrl(crmProperties.getSwagger().getLicenseUrl())
                .license(crmProperties.getSwagger().getLicense())
                .build();

    }
}

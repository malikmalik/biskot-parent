package com.sonepar.labs.biskot.exposition.rest.errors;

import java.util.ArrayList;
import java.util.List;

/**
 * View bean to transfer error message with a list of field errors
 *
 * @author JAU
 *
 */
public class ErrorMessage {
	private String message;

	private String description;

	private List<FieldErrorDetails> fieldErrors;

	private List<GlobalErrorDetails> globalErrors;

	public ErrorMessage() {

	}

	public ErrorMessage(String message) {
		this(message, null, null, null);
	}

	public ErrorMessage(String message, String description) {
		this(message, description, null, null);
	}

	public ErrorMessage(String message, String description, List<FieldErrorDetails> fieldErrors, List<GlobalErrorDetails> globalErrors) {
		this.message = message;
		this.description = description;
		this.fieldErrors = fieldErrors;
		this.globalErrors = globalErrors;
	}

	public void addFieldError(String objectName, String field, String message) {
		if (fieldErrors == null) {
			fieldErrors = new ArrayList<>();
		}
		fieldErrors.add(new FieldErrorDetails(objectName, field, message));
	}

	public void addGlobalError(String objectName, String message) {
		if (globalErrors == null) {
			globalErrors = new ArrayList<>();
		}
		globalErrors.add(new GlobalErrorDetails(objectName, message));
	}

	public String getMessage() {
		return message;
	}

	public String getDescription() {
		return description;
	}

	public List<FieldErrorDetails> getFieldErrors() {
		return fieldErrors;
	}

	public List<GlobalErrorDetails> getGlobalErrors() {
		return globalErrors;
	}

}

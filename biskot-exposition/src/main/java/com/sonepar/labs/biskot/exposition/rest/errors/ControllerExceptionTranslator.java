package com.sonepar.labs.biskot.exposition.rest.errors;

import com.sonepar.labs.biskot.domain.errors.BusinessException;
import com.sonepar.labs.biskot.domain.errors.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Controller advice to transform the exceptions to json structure
 *
 * @author JAU
 *
 */
@ControllerAdvice
public class ControllerExceptionTranslator {

	private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionTranslator.class);

	@Autowired
	private MessageSource messageSource;

	@InitBinder
	private void activateDirectFieldAccess(DataBinder dataBinder) {
		dataBinder.initDirectFieldAccess();
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorMessage processValidationError(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();
		List<ObjectError> globalErrors = result.getGlobalErrors();
		return processFieldErrors(fieldErrors, globalErrors);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorMessage processConstraintViolationValidationError(ConstraintViolationException ex) {
		return processConstraintViolationFieldErrors(ex.getConstraintViolations());
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorMessage processHttpMessageNotReadableError(HttpMessageNotReadableException ex) {
		return new ErrorMessage(ErrorConstants.ERR_HTTP_MESSAGE_NOT_READABLE, ex.getMessage());
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@ResponseBody
	public ErrorMessage processMethodNotSupportedError(HttpRequestMethodNotSupportedException ex) {
		return new ErrorMessage(ErrorConstants.ERR_METHOD_NOT_SUPPORTED, ex.getMessage());
	}

	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorMessage processMissingParameterError(MissingServletRequestParameterException ex) {
		return new ErrorMessage(ErrorConstants.ERR_PARAMETER, ex.getMessage());
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorMessage processTypeMismatchError(MissingServletRequestParameterException ex) {
		return new ErrorMessage(ErrorConstants.ERR_PARAMETER, ex.getMessage());
	}

	private ErrorMessage processConstraintViolationFieldErrors(Set<ConstraintViolation<?>> constraintViolations) {
		ErrorMessage errorInfo = new ErrorMessage(ErrorConstants.ERR_VALIDATION, "The structure is invalid.");

		constraintViolations.forEach(fe -> processConstraintViolation(errorInfo, fe));
		return errorInfo;
	}

	private void processConstraintViolation(ErrorMessage errorInfo, ConstraintViolation<?> fe) {
		List<String> fields = new ArrayList<String>();

		fe.getPropertyPath().forEach(path -> {
			fields.add(path.getName());
		});
		if (fields.size() > 1) {
			errorInfo.addFieldError(fields.get(0), fields.get(1), fe.getMessage());
		} else {
			if (fields.size() == 1) {
				errorInfo.addFieldError(null, fields.get(0), fe.getMessage());
			}
			LOGGER.warn("Validation error the fields size should be >1");
		}

	}

	private ErrorMessage processFieldErrors(List<FieldError> fieldErrors, List<ObjectError> globalErrors) {
		ErrorMessage errorInfo = new ErrorMessage(ErrorConstants.ERR_VALIDATION, "The structure is invalid.");
		fieldErrors.stream().forEach(fe -> errorInfo.addFieldError(fe.getObjectName(), fe.getField(), fe.getDefaultMessage()));
		globalErrors.stream().forEach(ge -> errorInfo.addGlobalError(ge.getObjectName(), ge.getDefaultMessage()));
		return errorInfo;
	}


	@ExceptionHandler(BusinessException.class)
	@ResponseBody
	public ResponseEntity<ErrorMessage> processBusinessException(BusinessException bex) {
		String code = "error." + bex.getMessage();
		BodyBuilder builder = ResponseEntity.status(bex.getHttpStatus());
		return builder.body(new ErrorMessage(code, messageSource.getMessage(code, bex.getParams(), code, Locale.FRENCH)));
	}

	@ExceptionHandler(TechnicalException.class)
	@ResponseBody
	public ResponseEntity<ErrorMessage> processTechnicalException(TechnicalException tex) {
		String code = "error." + tex.getMessage();
		BodyBuilder builder = ResponseEntity.status(tex.getHttpStatus());
		return builder.body(new ErrorMessage(code, messageSource.getMessage(code, tex.getParams(), code, Locale.FRENCH)));
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorMessage> processException(Exception ex) {
		BodyBuilder builder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
		return builder.body(new ErrorMessage(ErrorConstants.ERR_INTERNAL_SERVER));
	}


}

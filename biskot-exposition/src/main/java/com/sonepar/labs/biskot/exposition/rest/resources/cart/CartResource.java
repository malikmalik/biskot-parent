package com.sonepar.labs.biskot.exposition.rest.resources.cart;

import com.sonepar.labs.biskot.application.cart.create.CreateCartCommand;
import com.sonepar.labs.biskot.application.cart.create.CreateCartRequest;
import com.sonepar.labs.biskot.application.cart.get.GetCartQuery;
import com.sonepar.labs.biskot.application.cart.get.GetCartRequest;
import com.sonepar.labs.biskot.application.cart.update.AddItemCommand;
import com.sonepar.labs.biskot.application.cart.update.AddItemRequest;
import com.sonepar.labs.biskot.application.cqrs.command.CommandExecutorV2;
import com.sonepar.labs.biskot.application.cqrs.query.QueryExecutorV2;
import com.sonepar.labs.biskot.domain.cart.engine.CartResponse;
import com.sonepar.labs.biskot.domain.cart.model.Cart;
import com.sonepar.labs.biskot.exposition.rest.resources.cart.dto.AddItemRequestDto;
import com.sonepar.labs.biskot.exposition.rest.resources.cart.dto.CartResponseDto;
import com.sonepar.labs.biskot.exposition.rest.resources.cart.dto.ItemResponseDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/biskot")
public class CartResource {

    @Autowired

    private CommandExecutorV2 commandExecutor;
    @Autowired
    private QueryExecutorV2 queryExecutor;

    /**
     * PUT /carts/{cartId}/items : Add/update items in the cart.
     *
     * @param cartId Cart ID (required)
     * @param addItemRequest  (required)
     * @return Item added (status code 200)
     *         or Business rules have not been respected (status code 400)
     *         or Cart or product not found (status code 404)
     */
    @ApiOperation(value = "Add/update items in the cart.", nickname = "addItemToCart", notes = "", tags={ "Cart", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Item added"),
            @ApiResponse(code = 400, message = "Business rules have not been respected"),
            @ApiResponse(code = 404, message = "Cart or product not found") })
    @RequestMapping(value = "/carts/{cartId}/items",
            consumes = { "application/json" },
            method = RequestMethod.PUT)
    ResponseEntity<ItemResponseDto> addItemToCart(@ApiParam(value = "Cart ID",required=true) @PathVariable("cartId") String cartId,
                                         @ApiParam(value = "" ,required=true )  @Valid @RequestBody AddItemRequestDto addItemRequest) {

        commandExecutor.execute(AddItemCommand.class, AddItemRequest.builder()
                        .cartId(cartId)
                        .productId(addItemRequest.getProductId())
                        .quantity(addItemRequest.getQuantity())
                .build());
        ItemResponseDto itemResponseDto = null;
        return ResponseEntity.ok().body(itemResponseDto);
    }


    /**
     * POST /carts : Create a cart.
     *
     * @return Cart initialized (status code 200)
     */
    @ApiOperation(value = "Create a cart.", nickname = "createCart", notes = "", tags={ "Cart", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Cart initialized") })
    @RequestMapping(value = "/carts",
            method = RequestMethod.POST)
    ResponseEntity<String> createCart() {

        Cart cart = commandExecutor.execute(CreateCartCommand.class, CreateCartRequest.builder()
                .build());
        return ResponseEntity.ok().body(cart.getCartId());
    }


    /**
     * GET /carts/{cartId} : Retrieve a cart
     *
     * @param cartId Cart ID (required)
     * @return Cart retrieved (status code 200)
     *         or Cart not found (status code 404)
     */
    @ApiOperation(value = "Retrieve a cart", nickname = "getCart", notes = "", response = CartResponse.class, tags={ "Cart", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Cart retrieved", response = CartResponse.class),
            @ApiResponse(code = 404, message = "Cart not found") })
    @RequestMapping(value = "/carts/{cartId}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    ResponseEntity<CartResponseDto> getCart(@ApiParam(value = "Cart ID",required=true) @PathVariable("cartId") String cartId) {

        CartResponse cartResponse = queryExecutor.execute(GetCartQuery.class, GetCartRequest.builder()
                        .cartId(cartId)
                .build());
        CartResponseDto cartResponseDto = CartResponseDto.builder()
                .id(cartResponse.getCartId())
                .items(Optional.ofNullable(cartResponse).map(CartResponse::getItemDetails)
                        .orElse(Collections.emptyList()).stream().map(itemDetail -> ItemResponseDto.builder()
                                .productId(itemDetail.getProduct().getId())
                                .productLabel(itemDetail.getProduct().getLabel())
                                .unitPrice(Optional.ofNullable(itemDetail.getProduct().getUnitPrice()).map(BigDecimal::doubleValue)
                                        .orElse(0d))
                                .quantity(itemDetail.getQuantity())
                                .linePrice(Optional.ofNullable(itemDetail.getLinePrice()).map(BigDecimal::doubleValue)
                                        .orElse(0d))
                                .build())
                        .collect(Collectors.toList()))
                .totalPrice(Optional.ofNullable(cartResponse.getTotalPrice()).map(BigDecimal::doubleValue)
                        .orElse(0d))
                .build();
        return ResponseEntity.ok().body(cartResponseDto);
    }

}

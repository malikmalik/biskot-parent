package com.sonepar.labs.biskot.exposition.rest.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "crm", ignoreUnknownFields = false)
@Data
public class CrmProperties {

    private final Swagger swagger = new Swagger();

    public Swagger getSwagger() {
        return swagger;
    }

    @Data
    public static final class Swagger {

        private String title = "biskot API Documentation";

        private String description = "biskot API Documentation";

        private String version;

        private String termsOfServiceUrl;

        private String contactName;

        private String contactUrl;

        private String contactEmail;

        private String license;

        private String licenseUrl;

    }
}

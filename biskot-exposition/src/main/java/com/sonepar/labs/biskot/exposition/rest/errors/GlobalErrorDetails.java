package com.sonepar.labs.biskot.exposition.rest.errors;

/**
 * Provides field error details with an error message
 *
 * @author JAU
 *
 */
public class GlobalErrorDetails {
	private String objectName;

	private String message;

	public GlobalErrorDetails(String objectName, String message) {
		this.objectName = objectName;
		this.message = message;
	}

	public String getObjectName() {
		return objectName;
	}

	public String getMessage() {
		return message;
	}
}

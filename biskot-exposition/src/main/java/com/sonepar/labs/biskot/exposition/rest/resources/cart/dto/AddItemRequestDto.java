package com.sonepar.labs.biskot.exposition.rest.resources.cart.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AddItemRequestDto {

    private Long productId;
    private Integer quantity;
}

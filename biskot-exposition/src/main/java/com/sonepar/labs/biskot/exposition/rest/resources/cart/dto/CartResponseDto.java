package com.sonepar.labs.biskot.exposition.rest.resources.cart.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
@Builder
@ToString
public class CartResponseDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("items")
    @Valid
    private List<ItemResponseDto> items = new ArrayList<>();

    @JsonProperty("totalPrice")
    private Double totalPrice;
}

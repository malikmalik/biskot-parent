package com.sonepar.labs.biskot.infrastructure.sql.entities.cart.repository;

import com.sonepar.labs.biskot.domain.cart.model.Cart;
import com.sonepar.labs.biskot.domain.cart.repository.CartRepository;
import com.sonepar.labs.biskot.infrastructure.sql.entities.cart.mapper.CartMapperToDomain;
import com.sonepar.labs.biskot.infrastructure.sql.entities.cart.mapper.CartMapperToJpa;
import com.sonepar.labs.biskot.infrastructure.sql.entities.cart.model.CartEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CartRepositoryImpl implements CartRepository {

    @Autowired
    private CartJpaRepository cartJpaRepository;

    @Override
    @Transactional(value = "biskotTransactionManager", readOnly = true)
    public Cart findById(String cartId) {
        return cartJpaRepository.findById(cartId)
                .map(CartMapperToDomain::toDomain)
                .orElse(null);
    }

    @Override
    @Transactional(value = "biskotTransactionManager", propagation = Propagation.REQUIRED)
    public Cart saveCart(Cart cart) {
        CartEntity cartEntity = cartJpaRepository.save(CartMapperToJpa.toJpa(cart));
        return CartMapperToDomain.toDomain(cartEntity);
    }
}

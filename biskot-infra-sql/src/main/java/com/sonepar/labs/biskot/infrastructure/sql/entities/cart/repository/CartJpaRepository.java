package com.sonepar.labs.biskot.infrastructure.sql.entities.cart.repository;

import com.sonepar.labs.biskot.infrastructure.sql.entities.cart.model.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CartJpaRepository extends JpaRepository<CartEntity, String>, JpaSpecificationExecutor<CartEntity> {
}

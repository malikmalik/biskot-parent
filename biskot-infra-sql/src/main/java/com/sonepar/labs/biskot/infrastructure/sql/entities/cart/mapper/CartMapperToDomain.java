package com.sonepar.labs.biskot.infrastructure.sql.entities.cart.mapper;

import com.sonepar.labs.biskot.domain.cart.model.Cart;
import com.sonepar.labs.biskot.domain.cart.model.Item;
import com.sonepar.labs.biskot.infrastructure.sql.entities.cart.model.CartEntity;
import com.sonepar.labs.biskot.infrastructure.sql.entities.cart.model.ItemEntity;

import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

public class CartMapperToDomain {

    public static Cart toDomain(CartEntity cartEntity){
        return Optional.ofNullable(cartEntity).map(ce -> Cart.domainBuilder()
                        .cartId(ce.getCartId())
                        //.totalPrice(ce.getTotalPrice())
                        .items(Optional.ofNullable(ce).map(CartEntity::getItems).orElse(new HashSet<>())
                                .stream().map(CartMapperToDomain::toDomain).collect(Collectors.toList()))
                        .build())
                .orElse(null);
    }

    private static Item toDomain(ItemEntity entity) {
        return Optional.ofNullable(entity).map(jpa -> Item.domainBuilder()
                        .productId(jpa.getProductId())
                        .quantity(jpa.getQuantity())
                        .linePrice(jpa.getLinePrice())
                .build())
                .orElse(null);
    }
}

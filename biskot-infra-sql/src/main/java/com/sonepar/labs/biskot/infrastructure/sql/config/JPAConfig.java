package com.sonepar.labs.biskot.infrastructure.sql.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.validator.internal.engine.resolver.JPATraversableResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.sonepar.labs.biskot.infrastructure.sql",
        entityManagerFactoryRef = "biskotEntityManager",
        transactionManagerRef = "biskotTransactionManager")
@EntityScan(basePackages = {"com.sonepar.labs.biskot.infrastructure.sql.entities"}, basePackageClasses = Jsr310JpaConverters.class)
public class JPAConfig {

    @Autowired
    Environment env;

    // Datasource Properties
    @Value("${spring.datasource.driverClassName}")
    private String driverClassName;
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String userName;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${spring.datasource.minimumIdle:3}")
    private Integer minimumIdle;
    @Value("${spring.datasource.idleTimeout:60000}")
    private Long idleTimeout;

    // Hikari Properties
    @Value("${spring.datasource.hikari.poolName}")
    private String poolName;
    @Value("${spring.datasource.hikari.maximumPoolSize:30}")
    private Integer maximumPoolSize;
    @Value("${spring.datasource.hikari.connectionTimeout}")
    private Integer connectionTimeout;

    // Hibernate Properties
    @Value("${spring.jpa.database-platform}")
    private String databasePlatform;


    @Bean
    public DataSource biskotDataSource() {
        HikariConfig hikariConfig = new HikariConfig();

        // Datasource Properties
        hikariConfig.setPoolName(poolName);
        hikariConfig.setDriverClassName(driverClassName);
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(userName);
        hikariConfig.setPassword(password);

        // Hikari Properties
        hikariConfig.setMaximumPoolSize(maximumPoolSize);
        hikariConfig.setMinimumIdle(minimumIdle);
        hikariConfig.setIdleTimeout(idleTimeout);

        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean biskotEntityManager() {
        final LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(biskotDataSource());
        emf.setPackagesToScan(new String[]{"com.sonepar.labs.biskot.infrastructure.sql.entities"});

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        emf.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", databasePlatform);
        properties.put("hibernate.physical_naming_strategy", SpringPhysicalNamingStrategy.class.getName());
        properties.put("hibernate.implicit_naming_strategy", SpringImplicitNamingStrategy.class.getName());
        emf.setJpaPropertyMap(properties);

        return emf;
    }
    @Bean
    public PlatformTransactionManager biskotTransactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(biskotEntityManager().getObject());

        return jpaTransactionManager;
    }
}

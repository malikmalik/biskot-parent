package com.sonepar.labs.biskot.infrastructure.sql.entities.cart.mapper;

import com.sonepar.labs.biskot.domain.cart.model.Cart;
import com.sonepar.labs.biskot.domain.cart.model.Item;
import com.sonepar.labs.biskot.infrastructure.sql.entities.cart.model.CartEntity;
import com.sonepar.labs.biskot.infrastructure.sql.entities.cart.model.ItemEntity;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

public class CartMapperToJpa {

    public static CartEntity toJpa(Cart cart){
        CartEntity cartEntity = ofNullable(cart).map(c -> CartEntity.builder()
                        .cartId(c.getCartId())
                        .totalPrice(c.getTotalPrice())
                        .items(ofNullable(c).map(Cart::getItems).orElse(Collections.emptyList())
                                .stream().map(CartMapperToJpa::toDomain).collect(Collectors.toSet()))
                        .build())
                .orElse(null);
        addCartJpaParent(cartEntity);
        return cartEntity;
    }

    private static ItemEntity toDomain(Item item) {
        return Optional.ofNullable(item).map(i -> ItemEntity.builder()
                        .productId(i.getProductId())
                        .quantity(i.getQuantity())
                        .linePrice(i.getLinePrice())
                        .build())
                .orElse(null);
    }

    private static void addCartJpaParent(CartEntity cart) {
        ofNullable(cart.getItems()).orElse(new HashSet<>()).forEach(itemEntity -> {
            itemEntity.setCart(cart);
        });
    }
}

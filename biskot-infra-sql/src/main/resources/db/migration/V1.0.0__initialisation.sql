DROP TABLE IF EXISTS message;
DROP TABLE IF EXISTS dossier_client;

CREATE TABLE dossier_client (
    tech_id VARCHAR2(255 CHAR)  NOT NULL,
    reference VARCHAR2(250 CHAR),
    name VARCHAR2(250 CHAR) ,
    opening_date TIMESTAMP,
    PRIMARY KEY (tech_id)
);


CREATE TABLE message (
    tech_id VARCHAR2(255 CHAR)  NOT NULL,
    author_name VARCHAR2(250 CHAR) ,
    content VARCHAR2(250 CHAR),
    channel VARCHAR2(255 CHAR),
    date TIMESTAMP,
    id_dossier_client VARCHAR2(255 CHAR) NOT NULL,
    PRIMARY KEY (tech_id),
    FOREIGN KEY (id_dossier_client) REFERENCES dossier_client
);

-- Ajout de la clé etrangère vers la table Message
--ALTER TABLE message
--ADD CONSTRAINT fkkgitn9oltfepl761gso2ffzzz FOREIGN KEY (id_dossier_client) REFERENCES dossier_client;

DROP TABLE IF EXISTS cart;
DROP TABLE IF EXISTS item;

CREATE TABLE cart (
    cart_id VARCHAR2(255)  NOT NULL,
    total_price DECIMAL(19,2),
    PRIMARY KEY (cart_id)
);

CREATE TABLE item (
    tech_id VARCHAR2(255 CHAR)  NOT NULL,
    product_id NUMERIC,
    quantity NUMERIC,
    line_price DECIMAL(19,2),
    cart_id VARCHAR2(255 CHAR) NOT NULL,
    PRIMARY KEY (tech_id),
    FOREIGN KEY (cart_id) REFERENCES cart
);
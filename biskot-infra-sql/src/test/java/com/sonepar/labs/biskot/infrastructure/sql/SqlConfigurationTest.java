package com.sonepar.labs.biskot.infrastructure.sql;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SqlConfigurationTest {

    @SpringBootApplication
    static class TestSQLConfiguration {

    }

    @Test
    public void test() {

    }
}

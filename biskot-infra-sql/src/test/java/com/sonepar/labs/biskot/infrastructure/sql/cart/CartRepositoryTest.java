package com.sonepar.labs.biskot.infrastructure.sql.cart;

import com.sonepar.labs.biskot.domain.cart.model.Cart;
import com.sonepar.labs.biskot.domain.cart.model.Item;
import com.sonepar.labs.biskot.domain.cart.repository.CartRepository;
import com.sonepar.labs.biskot.infrastructure.sql.SqlConfigurationTest;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;

public class CartRepositoryTest extends SqlConfigurationTest {

    private static final String CART_ID = "7e076f36-faca-4ae2-b961-8890bd11a5a6";

    @Autowired
    private CartRepository cartRepository;

    @Test
    public void  testSaveCart() {
        // 1. Save Cart
        Cart cartSaved = cartRepository.saveCart(buildCart());
        Assertions.assertThat(cartSaved).isNotNull();

        // 2. Get Cart from DB
        Cart cart = cartRepository.findById(cartSaved.getCartId());
        Assertions.assertThat(cart).isNotNull();
        Assertions.assertThat(cart.getCartId()).isEqualTo(CART_ID);
        Assertions.assertThat(cart.getTotalPrice()).isEqualTo(BigDecimal.valueOf(18.38));
        Item item1 = cart.getItems().stream().filter(item -> item.getProductId().equals(1L)).findFirst().orElse(null);
        Assertions.assertThat(item1).isNotNull();
        Assertions.assertThat(item1.getProductId()).isEqualTo(1L);
        Assertions.assertThat(item1.getQuantity()).isEqualByComparingTo(2);
        Assertions.assertThat(item1.getLinePrice()).isEqualTo(BigDecimal.valueOf(6.22));

        Item item2 = cart.getItems().stream().filter(item -> item.getProductId().equals(2L)).findFirst().orElse(null);
        Assertions.assertThat(item2).isNotNull();
        Assertions.assertThat(item2.getProductId()).isEqualTo(2L);
        Assertions.assertThat(item2.getQuantity()).isEqualByComparingTo(4);
        Assertions.assertThat(item2.getLinePrice()).isEqualTo(BigDecimal.valueOf(12.16));

    }

    private Cart buildCart() {
        return Cart.domainBuilder()
                .cartId(CART_ID)
                //.totalPrice(BigDecimal.valueOf(18.38))
                .items(Arrays.asList(
                        Item.domainBuilder()
                                .productId(1L)
                                .quantity(2)
                                .linePrice(BigDecimal.valueOf(6.22))
                                .build(),
                        Item.domainBuilder()
                                .productId(2L)
                                .quantity(4)
                                .linePrice(BigDecimal.valueOf(12.16))
                                .build()
                ))
                .build();
    }
}
